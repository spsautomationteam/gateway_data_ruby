@intg
Feature:Verify ACH reports


  @get-ach-invalid-authorization
  Scenario: Verify report for get ach rejects with invalid authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}


  @get-ach-rejects
  Scenario: Verify report for get ach rejects
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects?startDate=2017-11-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending
    Then response code should be 200
    Then response should contain issue[D-36800]

  @get-ach-authorization-with-wrong_MID_MKEY
  Scenario: Verify report for get ach rejects with  authorization MID MKEY wrong
    Given I set Authorization header to Bearer SPS-DEV-GW:test.99999999997A.K3QD6YWYHFD
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidCredentials","errorDescription":"The client credentials supplied are invalid"}

  @get-ach-rejects-with-default-parameters
  Scenario: Verify report for get ach rejects with default parameters
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects
    Then response code should be 200
    Then response should contain "startDate":
    Then response should contain "endDate":
    Then response should contain "totalItemCount":
    Then response should contain "pageSize":
    Then response should contain "pageNumber":
    Then response should contain "items"

  @get-ach-rejects-with-invalid-sort-field
  Scenario: Verify report for get ach rejects with invalid sort field
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects?sortField=dd
    Then response code should be 400
    Then response should contain {"errorCode":"InvalidRequestData","errorDescription":"SortField must be a field in the result set"}

  @get-ach-rejects-with-startdate-enddate-same
  Scenario: Verify report for get ach rejects with startdate ,enddate same
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects?startDate=2018-03-01&endDate=2018-03-01
    Then response code should be 200
    Then response should contain {"startDate":"2018-03-01","endDate":"2018-03-01"
    Then response should contain "totalItemCount":
    Then response should contain "pageSize":
    Then response should contain "pageNumber":
    Then response should contain "items"


  @get-ach-rejects-with-ascending direction
  Scenario: Verify report for get ach rejects ascending
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects?sortDirection=Ascending
    Then response code should be 200
    Then response should contain "startDate":
    Then response should contain "endDate":
    Then response should contain "totalItemCount":
    Then response should contain "pageSize":

  @get-ach-rejects-with-descending sort direction
  Scenario: Verify report for get ach rejects descending
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ACH/Rejects?sortDirection=Descending
    Then response code should be 200
    Then response should contain "startDate":
    Then response should contain "endDate":
    Then response should contain "totalItemCount":
    Then response should contain "pageSize":