@intg
Feature:Verify Alert reports


  @get-chargeback-alert
  Scenario: Verify report for charge back alert
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Chargebacks?date=2017-08-04
    Then response code should be 200
    Then response should contain "gatewayId":"184614437832"
    Then response should contain "merchantId":"3948907861000002"
    Then response should contain "date":"2017-08-04
    Then response should contain "amount":


  @get-chargeback-alert-wrong-date
  Scenario: Verify report for charge back alert with wrong date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Chargebacks?date=abc
    Then response code should be 400
    Then response body path message should contain The request is invalid.
    Then response body path messageDetail should contain The parameters dictionary contains a null entry for parameter 'date' of non-nullable type 'System.DateTime'

  @get-chargeback-alert-wrong-authorization
  Scenario: Verify report for charge back alert with wrong authorization
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Chargebacks?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should contain Required Authorization header not present

  @get-chargeback-alert-wrong-authorization-mid/mkey
  Scenario: Verify report for charge back alert with wrong authorization with invalid mid/mkey
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Chargebacks?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid


  @get-deposit-alert
  Scenario: Verify report for deposit alert
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Deposits?date=2017-10-04
    Then response code should be 200
    Then response should contain [


  @get-deposit-alert-wrong-date
  Scenario: Verify report for deposit alert with wrong date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Deposits?date=abc
    Then response code should be 400
    Then response body path message should contain The request is invalid.
    Then response body path messageDetail should contain The parameters dictionary contains a null entry for parameter 'date' of non-nullable type 'System.DateTime'

  @get-deposit-alert-wrong-authorization
  Scenario: Verify report for deposit alert with wrong authorization
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Deposits?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should contain Required Authorization header not present

  @get-deposit-alert-wrong-authorization-mid/mkey
  Scenario: Verify report for deposit alert with wrong authorization with invalid mid/mkey
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Alerts/Deposits?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid