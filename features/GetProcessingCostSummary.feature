@intg
Feature:Verify different reports for Processing cost Summary


  @get-processing-cost-summary-no-date
  Scenario: Verify report for processing cost summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ProcessingCosts/Summary
    Then response code should be 200
    Then response should contain "totalSaleVolume":0.0
    Then response should contain "totalSaleCount":0
    Then response should contain "averageSaleVolume":0.0
    Then response should contain "averageCostPerSale":0.0
    Then response should contain "sixMonthAverageCostPerSale":0.0
    Then response should contain "averageCostPerSaleChangeType":"None"
    Then response should contain "averageCostPerSaleChangePercentage":0.0

  @get-processing-cost-summary-with-date
  Scenario: Verify report for processing cost summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ProcessingCosts/Summary?date=2017-08-05
    Then response code should be 200
    Then response should contain "totalSaleVolume":0.0
    Then response should contain "totalSaleCount":0
    Then response should contain "averageSaleVolume":0.0
    Then response should contain "averageCostPerSale":0.0
    Then response should contain "sixMonthAverageCostPerSale":0.0
    Then response should contain "averageCostPerSaleChangeType":"None"
    Then response should contain "averageCostPerSaleChangePercentage":0.0


  @get-processing-cost-summary-with-wrong-date
  Scenario: Verify report for processing cost summary with wrong date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/ProcessingCosts/Summary?date=abc
    Then response code should be 200
    Then response should contain "totalSaleVolume":0.0
    Then response should contain "totalSaleCount":0
    Then response should contain "averageSaleVolume":0.0
    Then response should contain "averageCostPerSale":0.0
    Then response should contain "sixMonthAverageCostPerSale":0.0
    Then response should contain "averageCostPerSaleChangeType":"None"
    Then response should contain "averageCostPerSaleChangePercentage":0.0


  @get-processing-cost-summary-with-wrong-authorization
  Scenario: Verify report for processing cost summary with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/ProcessingCosts/Summary?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Required Authorization header not present

  @get-processing-cost-summary-wrong-mid/mkey-in-authorization
  Scenario: Verify get countries  with wrong mid mkey in  authorization
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/ProcessingCosts/Summary?date=2017-08-04
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid


  @get-processing-cost-summary-with-wrong-sps-user
  Scenario: Verify report for processing cost summary with wrong sps user
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set SPS-Merchant header to abc
    When I get data for transaction /Data/ProcessingCosts/Summary?date=2017-08-05
    Then response code should be 200
    Then response should contain "totalSaleVolume":0.0
    Then response should contain "totalSaleCount":0
    Then response should contain "averageSaleVolume":0.0
    Then response should contain "averageCostPerSale":0.0
    Then response should contain "sixMonthAverageCostPerSale":0.0
    Then response should contain "averageCostPerSaleChangeType":"None"
    Then response should contain "averageCostPerSaleChangePercentage":0.0

  @get-processing-cost-summary-with-sps-user
  Scenario: Verify report for processing cost summary with sps-user
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set SPS-Merchant header to 403652
    When I get data for transaction /Data/ProcessingCosts/Summary?date=2017-08-05
    Then response code should be 200
    Then response should contain "totalSaleVolume":0.0
    Then response should contain "totalSaleCount":0
    Then response should contain "averageSaleVolume":0.0
    Then response should contain "averageCostPerSale":0.0
    Then response should contain "sixMonthAverageCostPerSale":0.0
    Then response should contain "averageCostPerSaleChangeType":"None"
    Then response should contain "averageCostPerSaleChangePercentage":0.0
