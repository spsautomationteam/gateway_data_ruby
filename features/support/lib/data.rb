class DataApi

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"


  #@base_url
  @reference_num = '0'
  @content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''
  @header_map={}
  @response_store=''

  #constructor,Reading yaml file
  def initialize()
    @header_map={}
    config = YAML.load_file('config.yaml')
    base_url = config['url']
    @auth = config['authorization']
    puts "@base_url :#{base_url}, @auth:#{@auth}"
    set_url base_url
  end

  #set authorization
  def set_authorization(auth)
    @authorization =auth
  end

  #set url
  def set_url(url)
    @url = url
  end

  #set header
  def set_header(header, value)
    puts "header : #{header}, value : #{value}"

    case value
      when "Authorization"
        @header_map[header]=@auth
      else
        @header_map[header]=value
    end

  end


  #set json body
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  #post txn
  def post(path)
    post_ref path, ''
  end

  #put txn
  def put(path)
    put_ref path, ''
  end

  #post txn with existing reference
  def post_existing_refnum(path)
    post_ref path, @reference_num
  end

  #patch txn from existing reference
  def patch_existing_refnum(path)
    patch_ref path, @reference_num
  end

  #post txn with any reference
  def post_ref(path, ref_num)
    full_url= @url+path
    puts "full_url:::::#{full_url}"

    if ((path.include? 'LineItems')||(path.include? 'ref_num'))
      full_url['ref_num']=ref_num
    elsif (!ref_num.empty?)
      full_url +='/'+ref_num
    end


    #Debug urls
    puts "path#{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"
    

    #Do post action and get response
    begin
      @response = RestClient.post full_url, @json_body, @header_map
      @response_store = @response
      puts "@response_store :#{@response_store}"
      #:Authorization => @authorization, :content_type => @content_type
      @reference_num = JSON.parse(@response.body)['reference']
      puts "@reference_num: #{@reference_num}"

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    rescue JSON::ParserError => err
      @response = @response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "Response: #{@response}"
  end

  def get_path
    get_ref path, ''
  end

  #delet with existing reference number
  def delete_existing_refnum path
    delete_ref path, @reference_num
  end

  #delete reference txn
  def delete_ref(path, ref_num)
    full_url= @url+path
    if (path.include? 'LineItems')
      full_url['ref_num']=ref_num
    elsif (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "Delete-> request path#{path}"
    puts "@full_url: #{full_url}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"
    #Do post action and get response

    begin
      @response = RestClient.delete full_url, @header_map
      # @reference_num = JSON.parse(@response.body)['reference']
      # puts "@reference_num: #{@reference_num}"
      puts "response:#{@response}"

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "Response: #{@response}"
  end

  #get reference txn
  def get_ref(path, ref_num)
    full_url= @url+path

    if ((path.include? 'LineItems')||(path.include? 'ref_num'))
      full_url['ref_num']=ref_num
    elsif (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "Get-> request path#{path}"
    puts "@full_url: #{full_url}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"

    puts "@headerarr : #{@header_arr}"
    #Do post action and get response

    begin
      @response = RestClient.get full_url, @header_map
      # @reference_num = JSON.parse(@response.body)['reference']
      # puts "@reference_num: #{@reference_num}"
      puts "Response:#{@response}"

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "Response: #{@response}"
  end

  #get txn with existing reference
  def get_existing_refnum(path)
    get_ref path, @reference_num
  end

  #put txn with any reference
  def put_ref(path, ref_num)
    full_url= @url+path

    if (!ref_num.empty?)
      full_url +='/'+ref_num
    end

    #Debug urls
    puts "path#{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"

    #Do post action and get response
    begin
      @response = RestClient.put full_url, @json_body, @header_map
        #      @reference_num = JSON.parse(@response.body)['reference']
        # puts "@reference_num: #{@reference_num}"

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "Response: #{@response}"
  end

  #patch txn
  def patch(path)
    patch_ref path, ''
  end

  #patch txn with any reference
  def patch_ref(path, ref_num)
    full_url= @url+path

    if (!ref_num.empty?)
      full_url +='/'+ref_num
      puts "ref_num:#{ref_num}"
    end

    puts "#patch_ref method "
    puts "path#{path}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"

    #Do post action and get response
    begin
      @response = RestClient.patch full_url, @json_body, @header_map
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
  end


  #To verify response parameter against value partial match
  def verify_response_params_contain(response_param, value)
    if @parsed_response[response_param].nil?
      expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

  #To check response is array
  def is_response_array
    puts @response
#expect(@response.kind_of?(Array)).to be_truethy , "Response is not an array as expected."
    expect(@response).to eql("[]"), "Response is not an array as expected."
  end

  #To verify empty response string
  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

  #Verify response string contains the value passed
  def verify_response_susb_string value
    if (!@response.nil?)
      expect(@response.include? value).to be_truthy, "Expected : #{value} ,got : null \nResponesbody:#{@response}"
    else
      expect(fail).to be_truthy, "Expected : #{value} ,got : empty response."
    end
  end

  #verify the existing reference number s existing
  def verify_existing_refnum
    puts "WSujith @@response_store:#{@response_store}"
    puts "@reference_num:#{@reference_num}"
    expect(@response_store.include? @reference_num.to_s).to be_truthy, "reference number mismatch"
  end

  #To verify  response param against value with exact match
  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if (response_param == "code")
      expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end
    end
  end

  #To verify response  headers
  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    ctype= @response.headers[:"#{header_param}"]
    puts "headers :#{@response.headers}"
    puts "ctype:#{ctype}"

    if (ctype.nil?)
      ctype ="null"
    end
    expect(ctype).to eql(value), "Expected : #{value} ,got : #{ctype} \nResponesbody:#{@parsed_response}"
  end

end