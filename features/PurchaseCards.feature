@intg
Feature:Verify different reports for purchase cards

  @post_line_items_visa&master_error
  Scenario: Verify post line item with visa nd master card json
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "retail": { "billing": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "shipping": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "level3": { "customernumber": "123456", "DestinationCountryCode": "USA", "vat": { "idnumber": "123", "amount": 1 }, "amounts": { "nationaltax": 1, "discount": 0, "duty": 0 } }, "amounts": { "total": 93, "tax": 1 }, "cardData": { "number": "4128123412341231", "expiration": "0520" } }, "transactionCode": "Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    Then response body path status should be Approved
    Then response body path isPurchaseCard should be true
    And I set body to {   "masterCard": [     {       "itemDescription": "string",       "productCode": "string",       "quantity": 0,       "unitOfMeasure": "string",       "unitCost": 0,       "taxAmount": 0,       "taxRate": 0,       "discountAmount": 0,       "alternateTaxIdentifier": "string",       "taxTypeApplied": "123",       "discountIndicator": "2",       "netGrossIndicator": "3",       "extendedItemAmount": 0,       "debitCreditIndicator": "1"     }   ],   "visa": [     {       "commodityCode": "string",       "itemDescription": "string",       "productCode": "string",       "quantity": 0,       "unitOfMeasure": "string",       "unitCost": 0,       "vatTaxAmount": 0,       "vatTaxRate": 0,       "discountAmount": 0,       "lineItemTotal": 0     }   ] }
    And I post data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 400
    Then response body path errorCode should be InvalidRequestData
    Then response body path errorDescription should be lineItems: Include one and only one of the following: MasterCard, Visa

  @post_get_delete_line_items_visa
  Scenario: Verify post line item , then get and delete line item with visa card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "retail": { "billing": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "shipping": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "level3": { "customernumber": "123456", "DestinationCountryCode": "USA", "vat": { "idnumber": "123", "amount": 1 }, "amounts": { "nationaltax": 1, "discount": 0, "duty": 0 } }, "amounts": { "total": 93.1, "tax": 1 }, "cardData": { "number": "4128123412341231", "expiration": "0520" } }, "transactionCode": "Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    Then response body path status should be Approved
    Then response body path isPurchaseCard should be true
    Given I set body to { "visa": [     {       "commodityCode": "Ab1",       "itemDescription": "Desc",       "productCode": "1234",       "quantity": 1,       "unitOfMeasure": "Kg",       "unitCost": 100,       "vatTaxAmount": 15,       "vatTaxRate": 12,       "discountAmount": 0,       "lineItemTotal": 0     }   ] }
    And I post data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 201
    Then response is empty
    Given I get data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 200
    Then response should contain {"commodityCode":"Ab1","itemDescription":"Desc","productCode":"1234","quantity":1,"unitOfMeasure":"Kg","unitCost":100.0000,"vatTaxAmount":15.0000,"vatTaxRate":12.0,"discountAmount":0.0000,"lineItemTotal":0.0000}
    Given I delete with existing ref num  data for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 204
    Then response is empty


  @post_get_delete_line_items_visa_text/json
  Scenario: Verify post line item with content type as text/json with
    Given I set Authorization header to Authorization
    And I set content-type header to text/json
    And I set body to { "retail": { "billing": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "shipping": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "level3": { "customernumber": "123456", "DestinationCountryCode": "USA", "vat": { "idnumber": "123", "amount": 1 }, "amounts": { "nationaltax": 1, "discount": 0, "duty": 0 } }, "amounts": { "total": 93, "tax": 1 }, "cardData": { "number": "4128123412341231", "expiration": "0520" } }, "transactionCode": "Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    Then response body path status should be Approved
    Then response body path isPurchaseCard should be true
    Given I set body to { "visa": [     {       "commodityCode": "Ab1",       "itemDescription": "Desc",       "productCode": "1234",       "quantity": 1,       "unitOfMeasure": "Kg",       "unitCost": 100,       "vatTaxAmount": 15,       "vatTaxRate": 12,       "discountAmount": 0,       "lineItemTotal": 0     }   ] }
    And I post data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 201
    Then response is empty
    Given I get data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 200
    Then response should contain {"commodityCode":"Ab1","itemDescription":"Desc","productCode":"1234","quantity":1,"unitOfMeasure":"Kg","unitCost":100.0000,"vatTaxAmount":15.0000,"vatTaxRate":12.0,"discountAmount":0.0000,"lineItemTotal":0.0000}
    Given I delete with existing ref num  data for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 204

  @post_get_delete_line_items_master
  Scenario: Verify post line item , then get and delete line item with master card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "retail": { "billing": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "shipping": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "level3": { "customernumber": "123456", "DestinationCountryCode": "USA", "vat": { "idnumber": "123", "amount": 1 }, "amounts": { "nationaltax": 1, "discount": 0, "duty": 0 } }, "amounts": { "total": 93, "tax": 1 }, "cardData": { "number": "4128123412341231", "expiration": "0520" } }, "transactionCode": "Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    Then response body path status should be Approved
    Then response body path isPurchaseCard should be true
    Given I set body to {   "masterCard": [     {       "itemDescription": "dec",       "productCode": "234",       "quantity": 0,       "unitOfMeasure": "Kg",       "unitCost": 0,       "taxAmount": 0,       "taxRate": 0,       "discountAmount": 0,       "alternateTaxIdentifier": "s",       "taxTypeApplied": "123",       "discountIndicator": "2",       "netGrossIndicator": "3",       "extendedItemAmount": 0,       "debitCreditIndicator": "1"     }   ] }
    And I post data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 201
    Then response is empty
    Given I get data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 200
    Then response should contain {"commodityCode":"","itemDescription":"dec","productCode":"234","quantity":0,"unitOfMeasure":"Kg","unitCost":0.0000,"vatTaxAmount":0.0000,"vatTaxRate":0.0,"discountAmount":0.0000,"lineItemTotal":0.0000}
    Given I delete with existing ref num  data for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 204
    Then response is empty

  @post_line_items_visa_wrong_reference
  Scenario: Verify post line item , then get and delete line item with visa card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "visa": [     {       "commodityCode": "Ab1",       "itemDescription": "Desc",       "productCode": "1234",       "quantity": 1,       "unitOfMeasure": "Kg",       "unitCost": 100,       "vatTaxAmount": 15,       "vatTaxRate": 12,       "discountAmount": 0,       "lineItemTotal": 0     }   ] }
    And I post data with reference as abcd for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 404
    Then response is empty

  @post_line_items_master_wrong_reference
  Scenario: Verify post line item , then get and delete line item with master card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "masterCard": [     {       "itemDescription": "dec",       "productCode": "234",       "quantity": 0,       "unitOfMeasure": "Kg",       "unitCost": 0,       "taxAmount": 0,       "taxRate": 0,       "discountAmount": 0,       "alternateTaxIdentifier": "s",       "taxTypeApplied": "123",       "discountIndicator": "2",       "netGrossIndicator": "3",       "extendedItemAmount": 0,       "debitCreditIndicator": "1"     }   ] }
    And I post data with reference as abcd for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 404
    Then response is empty

  @get_line_items_wrong_reference
  Scenario: Verify post line item , then get and delete line item with visa card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data with reference as abcd for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 404
    Then response is empty


  @delete_line_items_with_wrong_reference
  Scenario: Verify delete line item with wrong reference number
    Given I set Authorization header to Authorization
    And I delete data with reference as abcd for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 404
    Then response is empty

  @post_line_items_visa_ctype_application/x-www-form-urlencoded
  Scenario: Verify post line item with content type as application/x-www-form-urlencoded
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "retail": { "billing": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "shipping": { "name": "Henry", "address": "111 Main street", "city": "Manassas", "state": "VA", "postalCode": "20112" }, "level3": { "customernumber": "123456", "DestinationCountryCode": "USA", "vat": { "idnumber": "123", "amount": 1 }, "amounts": { "nationaltax": 1, "discount": 0, "duty": 0 } }, "amounts": { "total": 93, "tax": 1 }, "cardData": { "number": "4128123412341231", "expiration": "0520" } }, "transactionCode": "Sale" }
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    Then response body path status should be Approved
    Given I set Authorization header to Authorization
    And I set Content-Type header to application/x-www-form-urlencoded
    And I set body to {     "visa": [     {       "commodityCode": "Ab1",       "itemDescription": "Desc",       "productCode": "1234",       "quantity": 1,       "unitOfMeasure": "Kg",       "unitCost": 100,       "vatTaxAmount": 15,       "vatTaxRate": 12,       "discountAmount": 0,       "lineItemTotal": 0     }   ] }
    And I post data with existing ref num for transaction /Data/PurchaseCards/ref_num/LineItems
    Then response code should be 400
   # Then response body path message should be No request content was found
