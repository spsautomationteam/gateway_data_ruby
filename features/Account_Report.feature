@intg
Feature:Verify Account reports


  @update-account
  Scenario: Verify update account
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "xmlWeberviceEnabled": true,   "customerServiceNumber": "0011111111111" }
    When I put data for transaction /Data/Account
    Then response code should be 200
    Then response header content-type should be null
    Then response header pragma should be no-cache
    Then response is empty

  @update-account-invalid-json
  Scenario: Verify update account
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to abc123
    When I put data for transaction /Data/Account
    Then response code should be 400
    Then response body path message should be No request content was found


  @get-account-summary
  Scenario: Verify update account
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Account
    Then response code should be 200
    Then response should contain "merchantId":"3948907861000002","gatewayId":"999999999997","name":"The Candy Apple Company - Demo",
    Then response should contain "address":"16500 San Pedro Suite 400","city":"San Antonio","region":"TX","postalCode":"78232"
    Then response should contain "country":"US","customerServiceNumber":"0011111111111",
    Then response should contain "contactInfo":{"email":"sujith.ks@sonata-software.com","telephone":"210-402-4001","fax":"210-402-0432"},
    Then response should contain "services":{"bankcard":{"isLevel3Enabled":true,"batchesIncludePurchaseCards":false,"autoCloseHour":0,"isEnabled":true},
    Then response should contain "ach":{"isManualSettlementAllowed":true,"autoCloseHour":22,"isEnabled":true},
    Then response should contain "cash":{"isEnabled":false,"autoCloseHour":null
    Then response should contain "shoppingCart":{"isEnabled":
    Then response should contain "xmlWebServices":{"isEnabled":


  @get-account-summary-with-gatewayid
  Scenario: get account summary with gateway id
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Account/999999999997
    Then response code should be 200
    Then response body path gatewayId should be 999999999997
    Then response should contain {"merchantId":"3948907861000002","gatewayId":"999999999997","name":"The Candy Apple Company - Demo","address":"16500 San Pedro Suite 400","city":"San Antonio","region":"TX","postalCode":"78232","country":"US","customerServiceNumber":"0011111111111","contactInfo":{"email":"sujith.ks@sonata-software.com","telephone":"210-402-4001","fax":"210-402-0432"},"services":{"bankcard":{"isLevel3Enabled":true,"batchesIncludePurchaseCards":false,"autoCloseHour":0,"isEnabled":true},
    Then response should contain "ach":{"isManualSettlementAllowed":true,"autoCloseHour":22,"isEnabled":true},
    Then response should contain "cash":{"isEnabled":false,"autoCloseHour":null},
    Then response should contain "shoppingCart":{"isEnabled"
    Then response should contain "xmlWebServices":{"isEnabled":

  @get-account-summary-with-wrong-gatewayid
  Scenario: get account summary with wrong gateway id
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Account/abc123
    Then response code should be 404
    Then response is empty

  @get-account-summary-with-wrong-authorization
  Scenario: get account summary with gateway id
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Account/999999999997
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Required Authorization header not present

  @get-account-summary-authorization-wrong-mid/mkey
  Scenario: get account summary with gateway id
    Given I set Authorization header to 'Bearer SPS-DEV-GW:test.123.asd123'
    And I set content-type header to application/json
    When I get data for transaction /Data/Account/999999999997
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Authorization header not properly formed


  @get-account-payment-center-part
  Scenario: get account payment center parts
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Account/PaymentCenterParts
    Then response code should be 200
    Then response should contain []
