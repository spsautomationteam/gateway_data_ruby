@intg
Feature:Verify different reports for Funding end point


  @get-deposit-total
  Scenario: Verify get deposit total
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding/Totals?startDate=2017-12-01&endDate=2017-12-30
    Then response code should be 200
    Then response should contain {"count":10,"volume":2952.4,"routingNumber":"241070417","accountNumber":"*********3538"}


  @get-deposit-total-future-startdate
  Scenario: Verify get deposit total with stardate as future date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding/Totals?startDate=2026-12-01&endDate=2017-12-30
    Then response code should be 200
    Then response should contain {"count":0,"volume":0.0}


  @get-deposit-total-wrong-authorization
  Scenario: Verify get deposit total with wrong authorization code
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding/Totals?startDate=2017-12-01&endDate=2017-12-30
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @get-deposits
  Scenario: Verify get deposits
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding?startDate=2017-12-01&endDate=2017-12-30&pageSize13&pageNumber=1
    Then response code should be 200
    Then response should contain {"totalItemCount":10,"pageSize":500,"pageNumber":1,"href":"/Funding?pageSize=500&pageNumber=1","items":[{"merchantId":"3948907861000002","date":"2017-12-05T00:00:00","type":"Credit","amount":14.36,"accountNumber":"*********4770","routingNumber":"*****0288"},{"merchantId":"3948907861000002","date":"2017-12-05T00:00:00","type":"Credit","amount":11.09,"accountNumber":"*********3538","routingNumber":"*****0288"},{"merchantId":"3948907861000002","date":"2017-12-05T00:00:00","type":"Credit","amount":138.2,"accountNumber":"*********3538","routingNumber":"*****0417"},{"merchantId":"3948907861000002","date":"2017-12-05T00:00:00","type":"Credit","amount":729.64,"accountNumber":"*********3538","routingNumber":"*****0417"},{"merchantId":"3948907861000002","date":"2017-12-05T00:00:00","type":"Credit","amount":582.91,"accountNumber":"*********3538","routingNumber":"*****0417"},{"merchantId":"3948907861000002","date":"2017-12-04T00:00:00","type":"Credit","amount":14.36,"accountNumber":"*********4770","routingNumber":"*****0288"},{"merchantId":"3948907861000002","date":"2017-12-04T00:00:00","type":"Credit","amount":11.09,"accountNumber":"*********3538","routingNumber":"*****0288"},{"merchantId":"3948907861000002","date":"2017-12-04T00:00:00","type":"Credit","amount":138.2,"accountNumber":"*********3538","routingNumber":"*****0417"},{"merchantId":"3948907861000002","date":"2017-12-04T00:00:00","type":"Credit","amount":729.64,"accountNumber":"*********3538","routingNumber":"*****0417"},{"merchantId":"3948907861000002","date":"2017-12-04T00:00:00","type":"Credit","amount":582.91,"accountNumber":"*********3538","routingNumber":"*****0417"}]}

  @get-deposits-future-startdate
  Scenario: Verify get deposits with future start date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding?startDate=2026-12-01&endDate=2017-12-30&pageSize13&pageNumber=1
    Then response code should be 200
    Then response should contain {"totalItemCount":0,"pageSize":0,"pageNumber":0,"items":[]}

  @get-deposits-wrong-authorization
  Scenario: Verify get deposits with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Funding?startDate=2026-12-01&endDate=2017-12-30&pageSize13&pageNumber=1
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}