@intg
Feature:Verify different reports for bankcard-template


  @update-order-confirmation-bankcard-template
  Scenario: Verify update order confirmation bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "to": "customer@gmail.com",   "from": "merchant@gmail.com",   "fromFriendlyName": "Merchant",   "cc": "merchant2@gmail.com",   "subject": "Bankcard Order Confirmation",   "body": "Dear Customer,      Your order is delivered.This is conformation mail." }
    When I put data for transaction /Data/Templates/Bankcard/OrderConfirmation
    Then response code should be 200
    Then response is empty

  @get-order-confirmation-bankcard-template
  Scenario: Verify get order confirmation bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Templates/Bankcard/OrderConfirmation
    Then response code should be 200
    Then response should contain {"to":"customer@gmail.com","from":"merchant@gmail.com","fromFriendlyName":"Merchant","cc":"merchant2@gmail.com","subject":"Bankcard Order Confirmation","body":"Dear Customer,      Your order is delivered.This is conformation mail."}


  @deletet-order-confirmation-bankcard-template
  Scenario: Verify delete order confirmation bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I delete data for transaction /Data/Templates/Bankcard/OrderConfirmation
    Then response code should be 200
    Then response body path to should be <#customer.billing.email#>

  @update-receipt-bankcard-template
  Scenario: Verify update receipt bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "body": "merchant-name\r\n\r\n\r\nBill To:\r\n\tcustomer-billing-name\r\n\tcustomer-billing-address\r\n\tcustomer-billing-city, customer-billing-state customer-billing-zip\r\n        customer-billing-country\r\n\r\nShip To:\r\n\tcustomer-shipping-name\r\n\tcustomer-shipping-address\r\n\tcustomer-shipping-city, customer-shipping-state customer-shipping-zip\r\n        customer-shipping-country\t\r\n\r\n\r\nAccount  : customer-transaction-cardnumber\r\nTrx Type : customer-transaction-type \r\nOrder    : customer-transaction-ordernumber\r\nAuth     : customer-transaction-message\r\n\r\nAmount   : customer-transaction-subtotal\r\nTax      : customer-transaction-tax\r\nTotal    : customer-transaction-total\r\n\r\nCardmember Acknowledges Receipt Of\r\nGoods and/or Services In The Amount Of\r\nThe Total Shown Hereon And Agrees To\r\nPerform The Obligations Set Forth By The\r\nCardmember's Agreement With The Issuer\r\n\r\n\r\n\r\nX____________________________________________" }
    When I put data for transaction /Data/Templates/Bankcard/Receipt
    Then response code should be 200
    Then response is empty

  @get-receipt-bankcard-template
  Scenario: Verify get receipt bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Templates/Bankcard/Receipt
    Then response code should be 200
    Then response should contain merchant-name\r\n\r\n\r\nBill To:\r\n\tcustomer-billing-name\r\n\tcustomer-billing-address\r\n\tcustomer-billing-city, customer-billing-state customer-billing-zip\r\n        customer-billing-country\r\n\r\nShip To:\r\n\tcustomer-shipping-name\r\n\tcustomer-shipping-address\r\n\tcustomer-shipping-city, customer-shipping-state customer-shipping-zip\r\n        customer-shipping-country\t\r\n\r\n\r\nAccount  : customer-transaction-cardnumber\r\nTrx Type : customer-transaction-type \r\nOrder    : customer-transaction-ordernumber\r\nAuth     : customer-transaction-message\r\n\r\nAmount   : customer-transaction-subtotal\r\nTax      : customer-transaction-tax\r\nTotal    : customer-transaction-total\r\n\r\nCardmember Acknowledges Receipt Of\r\nGoods and/or Services In The Amount Of\r\nThe Total Shown Hereon And Agrees To\r\nPerform The Obligations Set Forth By The\r\nCardmember's Agreement With The Issuer\r\n\r\n\r\n\r\nX____________________________________________


  @delete-receipt-bankcard-template
  Scenario: Verify delete receipt bankcard-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I delete data for transaction /Data/Templates/Bankcard/Receipt
    Then response code should be 200
    Then response should contain <#merchant.name#>\r\n\r\n\r\nBill To:\r\n\t<#customer.billing.name#>\r\n\t<#customer.billing.address#>\r\n\t<#customer.billing.city#>, <#customer.billing.state#> <#customer.billing.zip#>\r\n        <#customer.billing.country#>\r\n\r\nShip To:\r\n\t<#customer.shipping.name#>\r\n\t<#customer.shipping.address#>\r\n\t<#customer.shipping.city#>, <#customer.shipping.state#> <#customer.shipping.zip#>\r\n        <#customer.shipping.country#>\t\r\n\r\n\r\nAccount  : <#customer.transaction.cardnumber#>\r\nTrx Type : <#customer.transaction.type#> \r\nOrder    : <#customer.transaction.ordernumber#>\r\nAuth     : <#customer.transaction.message#>\r\n\r\nAmount   : <#customer.transaction.subtotal#>\r\nTax      : <#customer.transaction.tax#>\r\nTotal    : <#customer.transaction.total#>\r\n\r\nCardmember Acknowledges Receipt Of\r\nGoods and/or Services In The Amount Of\r\nThe Total Shown Hereon And Agrees To\r\nPerform The Obligations Set Forth By The\r\nCardmember's Agreement With The Issuer\r\n\r\n\r\n\r\nX____________________________________________
#######################ACH#######################


  @update-order-confirmation-ach-template
  Scenario: Verify update order confirmation ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"to": "customer-billing-email",   "from": "merchant-email",   "fromFriendlyName": "merchant-name",   "cc": "merchant-email",   "subject": "Virtual Check Order Confirmation",   "body": "Dear customer-billing-name," }}    When I put data for transaction /Data/ach-templates/Bankcard/OrderConfirmation}
    When I put data for transaction /Data/Templates/ACH/OrderConfirmation
    Then response code should be 200
    Then response is empty


  @get-order-confirmation-ach-template
  Scenario: Verify get order confirmation ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Templates/ACH/OrderConfirmation
    Then response code should be 200
    Then response should contain {"to":"customer-billing-email","from":"merchant-email","fromFriendlyName":"merchant-name","cc":"merchant-email","subject":"Virtual Check Order Confirmation","body":"Dear customer-billing-name,"}


  @delete-order-confirmation-ach-template
  Scenario: Verify delete order confirmation ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I delete data for transaction /Data/Templates/ACH/OrderConfirmation
    Then response code should be 200
    Then response should contain {"to":"<#customer.billing.email#>","from":"<#merchant.email#>","fromFriendlyName":"<#merchant.name#>","cc":"<#merchant.email#>","subject":"Virtual Check Order Confirmation","body":"Dear <#customer.billing.name#>,\r\n\r\nThis is a confirmation of an online transaction placed with <#merchant.name#>.  \r\nThe transaction totaled $<#customer.transaction.total#> and will be processed to your account.\r\n\r\nOrder Number  : <#customer.transaction.ordernumber#>\r\n\r\n"}

  @update-receipt-ach-template
  Scenario: Verify update receipt ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "body": "<p>merchant-name Bill To: customer-billing-name customer-billing-address customer-billing-city, customer-billing-state customer-billing-zip customer-billing-country Ship To: customer-shipping-name customer-shipping-address customer-shipping-city, customer-shipping-state customer-shipping-zip customer-shipping-country Account : customer-transaction-cardnumber Trx Type : customer-transaction-type Order : customer-transaction-ordernumber Auth : customer-transaction-message Amount : customer-transaction-subtotal Tax : customer-transaction-tax Total : customer-transaction-total Cardmember Acknowledges Receipt Of Goods and/or Services In The Amount Of The Total Shown Hereon And Agrees To Perform The Obligations Set Forth By The Cardmember's Agreement With The Issuer X____________________________________________</p>" }
    When I put data for transaction /Data/Templates/ACH/Receipt
    Then response code should be 200
    Then response is empty

  @get-receipt-ach-template
  Scenario: Verify get receipt ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Templates/ACH/Receipt
    Then response code should be 200
    Then response should contain "<p>merchant-name Bill To: customer-billing-name customer-billing-address customer-billing-city, customer-billing-state customer-billing-zip customer-billing-country Ship To: customer-shipping-name customer-shipping-address customer-shipping-city, customer-shipping-state customer-shipping-zip customer-shipping-country Account : customer-transaction-cardnumber Trx Type : customer-transaction-type Order : customer-transaction-ordernumber Auth : customer-transaction-message Amount : customer-transaction-subtotal Tax : customer-transaction-tax Total : customer-transaction-total Cardmember Acknowledges Receipt Of Goods and/or Services In The Amount Of The Total Shown Hereon And Agrees To Perform The Obligations Set Forth By The Cardmember's Agreement With The Issuer X____________________________________________</p>


  @delete-receipt-ach-template
  Scenario: Verify delete receipt ach-template
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I delete data for transaction /Data/Templates/ACH/Receipt
    Then response code should be 200
    Then response should contain <#merchant.name#>\r\n\r\nBill To:\r\n                <#customer.billing.name#>\r\n                <#customer.billing.address#>\r\n                <#customer.billing.city#>, <#customer.billing.state#> <#customer.billing.zip#>\r\n        <#customer.billing.country#>\r\n\r\nShip To:\r\n                <#customer.shipping.name#>\r\n                <#customer.shipping.address#>\r\n                <#customer.shipping.city#>, <#customer.shipping.state#> <#customer.shipping.zip#>\r\n        <#customer.shipping.country#>       \r\n\r\n\r\nAccount  : <#customer.transaction.acct#>\r\nTrx Type : <#customer.transaction.type#> \r\nOrder    : <#customer.transaction.ordernumber#>\r\nAuth     : <#customer.transaction.message#>\r\n\r\nAmount   : <#customer.transaction.subtotal#>\r\nTax      : <#customer.transaction.tax#>\r\nTotal    : <#customer.transaction.total#>\r\n\r\nAccount member Acknowledges Receipt Of\r\nGoods and/or Services In The Amount Of\r\nThe Total Shown Hereon And Agrees To\r\nPerform The Obligations Set Forth By The\r\nAccount member's Agreement With The Issuer\r\n\r\n\r\n\r\nX____________________________________________

