@intg
Feature:Verify VT permission  reports


  @get-permissions
  Scenario: Verify report for VT - permissions
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Permissions/Transactions
    Then response code should be 200
    Then response should contain "allowVirtualTerminalSettings":true,
    Then response should contain "bankcardPermissions":{"allowManualTransactions":true,"manualTransactionsLimit":999999999.0000,"allowManualCredits":true,"manualCreditsLimit":0.0000,"allowRecurringTransactions":true,"allowBatchOperations":true},"
    Then response should contain achPermissions":{"allowManualTransactions":true,"manualTransactionsLimit":999999999.0000,"allowManualCredits":true,"manualCreditsLimit":0.0000,"allowRecurringTransactions":true,"allowBatchOperations":true},
    Then response should contain "allowReportsAndTransactionReview":true


  @get-permissions-wrong-authorization
  Scenario: Verify report for VT - permissions with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Permissions/Transactions
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Required Authorization header not present

  @get-permissions-wrong-mid/mkey
  Scenario: Verify report for VT - permissions with wrong mid /mkey
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Permissions/Transactions
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid