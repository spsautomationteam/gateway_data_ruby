@intg
Feature:Verify different reports for Batches end point


  @get-current-batch-summary
  Scenario: Verify get current batch summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/CurrentSummary
    Then response code should be 200
    Then response should contain {"summaryType":"Bankcard","debitCount"
    Then response should contain {"summaryType":"ACH","debitCount"
    Then response should contain {"summaryType":"PurchaseCard"

  @get-current-batch-summary-by-type-bankcard
  Scenario: Verify get current batch summary by type as bank card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard/CurrentSummary
    Then response code should be 200
    Then response should contain {"paymentType":"American Express","authCount"
    Then response should contain {"paymentType":"Discover","authCount
    Then response should contain {"paymentType":"MasterCard","authCount"
    Then response should contain {"paymentType":"Visa","authCount"

  @get-current-batch-summary-by-type-ach
  Scenario: Verify get current batch summary by type as ach
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH/CurrentSummary
    Then response code should be 200
    Then response should contain {"items":[{"paymentType":"ARC","authCount"
    Then response should contain {"paymentType":"CCD","authCount"
    Then response should contain {"paymentType":"PPD","authCount"
    Then response should contain {"paymentType":"RCK","authCount"
    Then response should contain {"paymentType":"TEL","authCount"
    Then response should contain {"paymentType":"WEB","authCount"
    Then response should contain {"paymentType":"WEB","authCount"

  @get-current-batch-summary-by-type-purchase-card
  Scenario: Verify get current batch summary by type as purchase card
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/PurchaseCard/CurrentSummary
    Then response code should be 200
    Then response should contain {"items":[{"paymentType":"MasterCard"
    Then response should contain {"paymentType":"Visa","authCount"

  @get-batch-totals
  Scenario: Verify get batch totals
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Totals
    Then response code should be 200
    Then response should contain {"count":
    Then response should contain "net"
    Then response should contain "volume"

  @get-batch-totals-type-bankcard
  Scenario: Verify get batch totals with service type as Bankcard
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard/Totals
    Then response code should be 200
    Then response should contain {"count":
    Then response should contain "net"
    Then response should contain "volume"

  @get-batch-totals-type-ach
  Scenario: Verify get batch totals with service type as ach
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH/Totals
    Then response code should be 200
    Then response should contain {"count":
    Then response should contain "net"
    Then response should contain "volume"

  @get-batch-totals-type-bankcard-wrong-authorization
  Scenario: Verify get batch totals with service type as Bankcard
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard/Totals
    Then response code should be 401
    Then response body path errorCode should contain InvalidHeaders
    Then response body path errorDescription should contain Required Authorization header not present


  @get-batch-totals-type-ach-wrong-authorization
  Scenario: Verify get batch totals with service type as ACH
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH/Totals
    Then response code should be 401
    Then response body path errorCode should contain InvalidHeaders
    Then response body path errorDescription should contain Required Authorization header not present


  @get-batch-totals-bankcard-future-startdate
  Scenario: Verify get batch totals with service type as bankcard,and startdate ad future date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard/Totals?startDate=2025-03-03
    Then response code should be 200
    Then response should contain {"count":0,"net":0.0,"volume":0.0}

  @get-batch-totals-ach-future-startdate
  Scenario: Verify get batch totals with service type as ach,and startdate ad future date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH/Totals?startDate=2025-03-03
    Then response code should be 200
    Then response should contain {"count":0,"net":0.0,"volume":0.0}

  @get-batches
  Scenario: Verify get batches
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":2
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Batches?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Batches?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2","items":[{"service":"Bankcard
    Then response should contain "terminalNumber":
    Then response should contain "date":
    Then response should contain "count":
    Then response should contain "net":
    Then response should contain "volume":

  @get-batches-future-startdate
  Scenario: Verify get batches with future start date
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches?startDate=2022-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2022-01-01","endDate":"2017-12-30","totalItemCount":0,"pageSize":0,"pageNumber":0,"items":[]}

  @get-batches-type-bankcard
  Scenario: Verify get batches with type as bankcard
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Batches/Bankcard?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Batches/Bankcard?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2","items":[{"service":"Bankcard
    Then response should contain "terminalNumber":
    Then response should contain "date":
    Then response should contain "count":
    Then response should contain "net":
    Then response should contain "volume":

  @get-batches-type-bankcard-future-startdate
  Scenario: Verify get batches with type as bankcard with start date as future
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/Bankcard?startDate=2025-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2025-01-01","endDate":"2017-12-30","totalItemCount":0,"pageSize":0,"pageNumber":0,"items":[]}


  @get-batches-type-ach
  Scenario: Verify get batches with type as ach
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Batches/ACH?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Batches/ACH?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2","items":[{"service":"ACH"
    Then response should contain "file":
    Then response should contain "reference":
    Then response should contain "date":
    Then response should contain "count":
    Then response should contain "net":
    Then response should contain "volume":


  @get-batches-type-ach-future-startdate
  Scenario: Verify get batches with type as ach with start date as future
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Batches/ACH?startDate=2025-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=service
    Then response code should be 200
    Then response should contain {"startDate":"2025-01-01","endDate":"2017-12-30","totalItemCount":0,"pageSize":0,"pageNumber":0,"items":[]}


  @get-batch-with-reference
  Scenario: Verify get batch with reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 522281.8    },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response body path status should be Approved
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "settlementType": "Bankcard"   }
    And I post data for transaction /Bankcard/Batches/Current
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data with existing ref num for transaction /Data/Batches/ref_num/Summary
    Then response code should be 200
    Then response should contain {"items":[
    Then response should contain "paymentType":"Visa
    Then response should contain "authCount":
    Then response should contain "authTotal":
    Then response should contain "saleCount":
    Then response should contain "saleTotal":
    Then response should contain "creditCount":
    Then response should contain "creditTotal":
    Then response should contain "totalCount":
    Then response should contain "totalVolume":

  @get-batch-with-invalid-reference
  Scenario: Verify get batch with invalid reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Batches/abc123/Summary
    Then response code should be 200
    Then response should contain {"items":[
    Then response should contain "authCount":
    Then response should contain "authTotal":
    Then response should contain "saleCount":
    Then response should contain "saleTotal":
    Then response should contain "creditCount":
    Then response should contain "creditTotal":
    Then response should contain "totalCount":
    Then response should contain "totalVolume":

  @get-batch-with-existing-reference-invalid-authorization
  Scenario: Verify get batch with reference
    Given I set Authorization header to abc
    And I set content-type header to application/json
    And I get data for transaction /Data/Batches/Q9BLUB8X2L/Summary
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}


  @get-batch-details-with-reference
  Scenario: Verify get batch details with reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 9381.13    },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response body path status should be Approved
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "settlementType": "Bankcard"   }
    And I post data for transaction /Bankcard/Batches/Current
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data with existing ref num for transaction /Data/Batches/ref_num
    Then response code should be 200
    Then response should contain "service":"Bankcard","reference"
    Then response should contain pageNumber=1
    Then response should contain "gatewayId":"999999999997"
    Then response should contain "service":"Bankcard","status":"Settled","transactionCode":"Sale"


  @get-batch-details-with-invalid-reference
  Scenario: Verify get details with invalid reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Batches/abc123
    Then response code should be 200
    Then response should contain  defect D-36866

  @get-batch-details-with-existing-reference-invalid-authorization
  Scenario: Verify get batch details with reference
    Given I set Authorization header to abc
    And I set content-type header to application/json
    And I get data for transaction /Data/Batches/Q9BLUB8X2L
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}