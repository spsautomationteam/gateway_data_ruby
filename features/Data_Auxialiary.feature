@intg
Feature:Verify  Auxiliary gateway data end point

  @get_countries
  Scenario: Verify get countries  end point.
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Countries
    Then response code should be 200
    And response header content_type should be application/json; charset=utf-8
    And response should contain [{"isoFullName":"AALAND ISLANDS","isoNumericCode":"248","isoAlpha3Code":"ALA"}

  @get_countries-wrong-authorization
  Scenario: Verify get countries  with wrong authorization
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Countries
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Required Authorization header not present

  @get_countries-wrong-mid/mkey-in-authorization
  Scenario: Verify get countries  with wrongmid mkey in  authorization
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Countries
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid

  @get_state_provinces
  Scenario: Verify get state/provinces  end point.
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Subdivisions?countryCode=01
    Then response code should be 200
    And response header content_type should be application/json; charset=utf-8
    And response should contain [

  @get_state_provinces-with-empty-country-code
  Scenario: Verify get state/provinces  end point with empty country code
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Subdivisions
    Then response code should be 200
    And response header content_type should be application/json; charset=utf-8
    And response should contain [{"name":"Alabama","abbreviation":"AL"},{

  @get_state_provinces-wrong-country-code
  Scenario: Verify get state/provinces  end point.
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Subdivisions?countryCode=Abc
    Then response code should be 200
    And response header content_type should be application/json; charset=utf-8
    And response should contain []

  @get_countries-wrong-authorization
  Scenario: Verify get countries  with wrong authorization
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Subdivisions?countryCode=01
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should be Required Authorization header not present

  @get_countries-wrong-mid/mkey-in-authorization
  Scenario: Verify get countries  with wrong mid mkey in  authorization
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Subdivisions?countryCode=01
    Then response code should be 401
    Then response body path errorCode should be InvalidCredentials
    Then response body path errorDescription should be The client credentials supplied are invalid


  @get-ach-originators
  Scenario: Verify get ach originators  end point.
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Originators
    And response header content_type should be application/json; charset=utf-8
    And response should contain [{"id":"1084361710","description":"Washington","type":"PPD"}


  @get-ach-originators-wrong-authorization
  Scenario: Verify get ach originators  end point with wrong authorization.
    Given I set Authorization header to abc123
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Originators
    And response body path errorCode should be InvalidHeaders
    And response body path errorDescription should be Required Authorization header not present


  @get-ach-originators-wrong-mid/mkey
  Scenario: Verify get ach originators  end point with wrong mid/mkey.
    Given I set Authorization header to Bearer SPS-DEV-GW:test.123.abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Auxiliary/Originators
    And response body path errorCode should be InvalidCredentials
    And response body path errorDescription should be The client credentials supplied are invalid