@intg
Feature:Verify reports Chargeback


  @get-chargeback-totals
  Scenario: Verify get charge back totals
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    When I get data for transaction /Data/Chargebacks/Totals?startDate=2017-01-01&endDate=2017-12-30
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01
    Then response should contain "endDate":"2017-12-30
    Then response should contain "totalCount":
    Then response should contain "totalAmount":
    Then response should contain "resolvedAmount":
    Then response should contain "unresolvedCount":
    Then response should contain "unresolvedAmount":


  @get-unresolved-chargeback-datetype-as-received-date
  Scenario: Verify get unresolved chargebacks with datetype as 'ReceivedDate'
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    When I get data for transaction /Data/Chargebacks/UnresolvedByReceivedDate?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=transactionCode
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2"
    Then response should contain "caseNumber":
    Then response should contain "caseType":"First Chargeback",
    Then response should contain "transactionCode":"Retail Sale",
    Then response should contain "cardNumber":"5148XXXXXXXX9978",
    Then response should contain "transactionDate":"2017-01-03T00:00:00",
    Then response should contain "receivedDate":"2017-01-03T00:00:00",
    Then response should contain "amount":175.0}]}

  @get-unresolved-chargeback-datetype-as-transaction-date
  Scenario: Verify get unresolved chargebacks with datetype as 'TransactionDate'
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    When I get data for transaction /Data/Chargebacks/UnresolvedByTransactionDate?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=transactionCode
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2"
    Then response should contain "caseNumber":
    Then response should contain "caseType":"First Chargeback",
    Then response should contain "transactionCode":"Retail Sale",
    Then response should contain "cardNumber":"5148XXXXXXXX9978",
    Then response should contain "transactionDate":"2017-01-03T00:00:00",
    Then response should contain "receivedDate":"2017-01-03T00:00:00",
    Then response should contain "amount":175.0}]}

  @get-unresolved-chargeback-datetype-as-resolved-date
  Scenario: Verify get unresolved chargebacks with datetype as 'ResolvedDate'
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    When I get data for transaction /Data/Chargebacks/UnresolvedByResolvedDate?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1&sortDirection=Ascending&sortField=transactionCode
    Then response code should be 200
    Then response should contain {"startDate":"2017-01-01","endDate":"2017-12-30","totalItemCount":
    Then response should contain "pageSize":1,"pageNumber":1,"href":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=1","next":"/Chargebacks/Unresolved?startDate=2017-01-01&endDate=2017-12-30&pageSize=1&pageNumber=2"
    Then response should contain "caseNumber":
    Then response should contain "caseType":"First Chargeback",
    Then response should contain "transactionCode":"Retail Sale",
    Then response should contain "cardNumber":"5148XXXXXXXX9978",
    Then response should contain "transactionDate":"2017-01-03T00:00:00",
    Then response should contain "receivedDate":"2017-01-03T00:00:00",
    Then response should contain "amount":175.0}]}