@intg
Feature:Verify different reports in merchant Statements

  @get_merchant_statements
  Scenario: Verify get merchant statements
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements
    Then response code should be 200
    And response should contain {"href":"/MerchantStatements/Bankcard/
    And response should contain "type":"Bankcard","year":
    And response should contain "month":

  @get_merchant_statements_by_mid
  Scenario: Verify get merchant statements by mid
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/3948907861000002
    Then response code should be 200
    And response should contain [{"href":"/MerchantStatements/3948907861000002
    And response should contain "type":
    And response should contain "year":
    And response should contain "month":
    And response should contain "merchantId":

  @get_merchant_statements_service(Bankcard)_year_month
  Scenario: Verify get merchant statements with service(bankcard),year and month
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/Bankcard/2017/10
    Then response code should be 200
    And response should contain issue

  @get_merchant_statements_service(ACH)_year_month
  Scenario: Verify get merchant statements with service(ach),year and month
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/ACH/2017/7
    Then response code should be 200
    And response should contain issue

  @get_merchant_statements_UNOMID_service(Bankcard)_year_month
  Scenario: Verify get merchant statements with service(bankcard),year and month
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/3948907861000002/Bankcard/2017/10
    Then response code should be 200
    And response should contain issue

  @get_merchant_statements_UNOMID_service(ACH)_year_month
  Scenario: Verify get merchant statements with service(ach),year and month
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/3948907861000002/ACH/2017/7
    Then response code should be 200
    And response should contain ss


  @get_EFT_merchant_statements_statement_id
  Scenario: Verify get eft merchant statements with statement id
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/EFT/1
    Then response code should be 200
    And response should contain Need to clarify statement id

  @get_EFT_merchant_statements_mid_statement_id
  Scenario: Verify get eft merchant statements with statement id and UNO MID
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/MerchantStatements/EFT/999999999997/1
    Then response code should be 200
    And response should contain  Need to clarify statement id