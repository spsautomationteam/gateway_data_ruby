And(/^I set Authorization header with MID as (.*) and MKEY as (.*)$/) do |mid, mkey|
  auth ='Bearer SPS-DEV-GW:test.'+mid+'.'+mkey
  @data.set_header 'Authorization', auth
end

And(/^I set url as (.*)$/) do |url|
  @data.set_url url
end

And(/^I set (.*) header to (.*)$/) do |header, value|
  @data.set_header header, value
end

And(/^I set body to (.*)$/) do |json|
  @data.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @data.post path
end

And(/^I put data for transaction (.*)$/) do |path|
  @data.put path
end

And(/^I get data with existing ref num for transaction (.*)$/) do |path|
  @data.get_existing_refnum path
end

And(/^I get data for transaction (.*)$/) do |path|
  @data.get_ref path, ''
end

And(/^I patch data for transaction (.*)$/) do |path|
  @data.patch path
end

And(/^I delete data for transaction (.*)$/) do |path|
  @data.delete_ref path, ''
end

And(/^I delete with existing ref num  data for transaction (.*)$/) do |path|
  @data.delete_existing_refnum path
end

And(/^I delete data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @data.delete_ref path, reference
end


And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @data.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @data.verify_response_params param, val
end

Then(/^I verify existing reference number in response$/) do
  @data.verify_existing_refnum
end

Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @data.verify_response_params_contain param, val
end

And(/^I patch data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @data.patch_ref path, reference
end

And(/^I post data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @data.post_ref path, reference
end

And(/^I get data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @data.get_ref path, reference
end

And(/^I post data with existing ref num for transaction (.*)$/) do |path|
  @data.post_existing_refnum path
end

And(/^I patch data with existing ref num for transaction (.*)$/) do |path|
  @data.delete_ref path, @reference_num
end

Then(/^response code should be (.*)$/) do |responsecode|
  @data.verify_response_params "code", responsecode
end

Then(/^response should contain (.*)$/) do |value|
  @data.verify_response_susb_string value
end

Then(/^response is empty$/) do
  @data.verify_response_empty
end
Then(/^response is Array?$/) do
  @data.is_response_array
end