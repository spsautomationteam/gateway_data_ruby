@intg
Feature:Verify reports of EMV feature


  @post_offline_decline
  Scenario: Verify post offline decline
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    And I set body to {   "name": "string",   "transactionCode": "Sale",   "sourceId": 0,   "emvTags": {     "50": "string",     "82": "string",     "84": "string",     "95": "string",     "4F": "string",     "5A": "string",     "5F2A": "string",     "5F34": "string",     "8A": "string",     "9A": "string",     "9B": "string",     "9F02": "string",     "9F03": "string",     "9F07": "string",     "9F0D": "string",     "9F0E": "string",     "9F0F": "string",     "9F10": "string",     "9F11": "string",     "9F12": "string",     "9F1A": "string",     "9F21": "string",     "9F26": "string",     "9F27": "string",     "9F34": "string",     "9F36": "string",     "9F37": "string",     "9F39": "string"   },   "terminalActionCodes": {     "default": "string",     "denial": "string",     "online": "string"   } }
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 200
    Then response is empty

  @post_offline_decline_text_json
  Scenario: Verify post offline decline_text_json
    Given I set Authorization header to Authorization
    And I set Content-type header to text/json
    And I set body to {   "name": "string",   "transactionCode": "Sale",   "sourceId": 0,   "emvTags": {     "50": "string",     "82": "string",     "84": "string",     "95": "string",     "4F": "string",     "5A": "string",     "5F2A": "string",     "5F34": "string",     "8A": "string",     "9A": "string",     "9B": "string",     "9F02": "string",     "9F03": "string",     "9F07": "string",     "9F0D": "string",     "9F0E": "string",     "9F0F": "string",     "9F10": "string",     "9F11": "string",     "9F12": "string",     "9F1A": "string",     "9F21": "string",     "9F26": "string",     "9F27": "string",     "9F34": "string",     "9F36": "string",     "9F37": "string",     "9F39": "string"   },   "terminalActionCodes": {     "default": "string",     "denial": "string",     "online": "string"   } }
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 200
    Then response is empty

  @get_emv_receipt_details
  Scenario: Verify get emv receipt details
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/EMV/Receipts/QA200
    Then response code should be 200
    Then response body path message should be Need clarification

  @get_emv_receipt_details-wrong-authorization
  Scenario: Verify get emv receipt details with wrong authorization
    Given I set Authorization header to abcd
    And I set content-type header to application/json
    When I get data for transaction /Data/EMV/Receipts/QA200
    Then response code should be 401
    Then response should contain "errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @get_emv_receipt_details-wrong-reference
  Scenario: Verify get emv receipt details with wrong reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/EMV/Receipts/QA200
    Then response code should be 404
    Then response is empty

  @post_offline_decline_wrong_json
  Scenario: Verify post offline decline
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    And I set body to abcd
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 401
    Then response body path errorCode should be AccessTokenInvalid
    Then response body path errorDescription should be The scopes authorized by the current access token do not match those required for this endpoint


  @post_offline_decline_empty_json
  Scenario: Verify post offline decline empty json
    Given I set Authorization header to Authorization
    And I set Content-type header to application/json
    And I set body to {}
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 401
    Then response body path errorCode should be AccessTokenInvalid
    Then response body path errorDescription should be The scopes authorized by the current access token do not match those required for this endpoint

  @post_offline_decline_wrong_authorization
  Scenario: Verify post offline decline with wrong authorization value
    Given I set Authorization header to abc
    And I set Content-type header to application/json
    And I set body to {   "name": "string",   "transactionCode": "Sale",   "sourceId": 0,   "emvTags": {     "50": "string",     "82": "string",     "84": "string",     "95": "string",     "4F": "string",     "5A": "string",     "5F2A": "string",     "5F34": "string",     "8A": "string",     "9A": "string",     "9B": "string",     "9F02": "string",     "9F03": "string",     "9F07": "string",     "9F0D": "string",     "9F0E": "string",     "9F0F": "string",     "9F10": "string",     "9F11": "string",     "9F12": "string",     "9F1A": "string",     "9F21": "string",     "9F26": "string",     "9F27": "string",     "9F34": "string",     "9F36": "string",     "9F37": "string",     "9F39": "string"   },   "terminalActionCodes": {     "default": "string",     "denial": "string",     "online": "string"   } }
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 401
    Then response body path errorCode should be InvalidHeaders
    Then response body path errorDescription should contain Required Authorization header not present

  @post_offline_decline_application/x-www-form-urlencoded
  Scenario: Verify post offline decline_application/x-www-form-urlencoded
    Given I set Authorization header to Authorization
    And I set Content-type header to application/x-www-form-urlencoded
    And I set body to {   "name": "string",   "transactionCode": "Sale",   "sourceId": 0,   "emvTags": {     "50": "string",     "82": "string",     "84": "string",     "95": "string",     "4F": "string",     "5A": "string",     "5F2A": "string",     "5F34": "string",     "8A": "string",     "9A": "string",     "9B": "string",     "9F02": "string",     "9F03": "string",     "9F07": "string",     "9F0D": "string",     "9F0E": "string",     "9F0F": "string",     "9F10": "string",     "9F11": "string",     "9F12": "string",     "9F1A": "string",     "9F21": "string",     "9F26": "string",     "9F27": "string",     "9F34": "string",     "9F36": "string",     "9F37": "string",     "9F39": "string"   },   "terminalActionCodes": {     "default": "string",     "denial": "string",     "online": "string"   } }
    When I post data for transaction /Data/EMV/OfflineDeclines
    Then response code should be 200
    Then response is empty