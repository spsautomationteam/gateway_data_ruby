@intg
Feature:Verify different reports for Transactions end point

  @get_transactions
  Scenario: Verify get transactions
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions
    Then response code should be 200
    Then response should contain "href":"/Transactions?startDate
    Then response should contain "next":"/Transactions?startDate=
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"


  @get_transactions-different-status
  Scenario Outline: Verify get transactions with different status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions?gatewayId=999999999997&status=<status>
    Then response code should be 200
    Then response should contain "href":"/Transactions?startDate
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"
    Then response should contain "gatewayId":"999999999997"
    Then response should contain "status":"<status>"
    Examples:
      | status   |
      | Declined |
      | Batch    |
      | Settled  |


  @get_transactions-Expired
  Scenario: Verify get transactions with Expired status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions?gatewayId=999999999997&status=Expired
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"

  @get_transactions-wrong-authorization
  Scenario: Verify get transactions with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @get-transactions-byType
  Scenario: Verify get transactions by Type of service
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/Bankcard
    Then response code should be 200
    Then response should contain "service":"Bankcard"
    Then response should contain "href":"/Transactions/Bankcard?startDate
    Then response should contain "next":"/Transactions/Bankcard?startDate=
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"


  @get-transactions-byType-different-status
  Scenario Outline: Verify get transactions by Type of service with different status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/Bankcard?gatewayId=999999999997&status=<status>
    Then response code should be 200
    Then response should contain "service":"Bankcard"
    Then response should contain "href":"/Transactions/Bankcard?startDate
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"
    Then response should contain "gatewayId":"999999999997"
    Then response should contain "status":"<status>"
    Examples:
      | status   |
      | Declined |
      | Batch    |
      | Settled  |


  @get-transactions-byType-Expired
  Scenario: Verify get transactions by Type of service with Expired status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/Bankcard?gatewayId=999999999997&status=Expired
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"

  @get-transactions-byType-wrong-authorization
  Scenario: Verify get transactions by Type of service with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/Bankcard
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}


  @get-transactions-byType(ACH)
  Scenario: Verify get transactions by Type of service
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/ACH
    Then response code should be 200
    Then response should contain "service":"ACH"
    Then response should contain "href":"/Transactions/ACH?startDate
    Then response should contain "next":"/Transactions/ACH?startDate=
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"


  @get-transactions-byType(ACH)-different-status
  Scenario Outline: Verify get transactions by Type of service with different status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/ACH?gatewayId=999999999997&status=<status>
    Then response code should be 200
    Then response should contain "service":"ACH"
    Then response should contain "href":"/Transactions/ACH?startDate
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"
    Then response should contain "paymentType"
    Then response should contain "authCount"
    Then response should contain "authTotal"
    Then response should contain "saleCount"
    Then response should contain "saleTotal"
    Then response should contain "creditCount"
    Then response should contain "creditTotal"
    Then response should contain "totalCount"
    Then response should contain "totalVolume"
    Then response should contain "gatewayId":"999999999997"
    Then response should contain "status":"<status>"
    Examples:
      | status   |
      | Declined |
      | Batch    |
      | Settled  |


  @get-transactions-byType(ACH)-Expired
  Scenario: Verify get transactions by Type of service with Expired status
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/ACH?gatewayId=999999999997&status=Expired
    Then response should contain "startDate"
    Then response should contain "endDate"
    Then response should contain "totalItemCount"
    Then response should contain "pageSize"
    Then response should contain "pageNumber"

  @get-transactions-byType(ACH)-wrong-authorization
  Scenario: Verify get transactions by Type of service with wrong authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    And I get data for transaction /Data/Transactions/ACH
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}


  @get_transaction_details
  Scenario: Verify get transaction details
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 661.321   },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response body path status should be Approved
    And I get data for transaction /Data/Transactions/ref_num
    Then I verify existing reference number in response

  @get_transaction_details_by_type_bankcard
  Scenario: Verify get transaction details by type both service(bankcard) & reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 100.1   },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    When I get data with existing ref num for transaction /Data/Transactions/Bankcard/ref_num
    Then response code should be 200
    And response body path status should be Batch
    And response body path service should be Bankcard
    Then I verify existing reference number in response

  @get_transaction_details_by_type_ach
  Scenario: Verify get transaction details by type both service(ach) & reference
    Given I set Authorization header to Authorization
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "transactionCode": "Sale", "transactionClass": "CCD" , "originatorId": "string", "amounts": { "total": 65 }, "account": { "type": "Checking", "routingNumber": "123123123", "accountNumber": "1234567890" }, "customer": { "dateOfBirth": "2016-01-29T05:49:05.703Z", "ssn": "123456789", "license": { "number": "string", "stateCode": "VA" }, "ein": "test", "email": "achtest@gmail.com", "telephone": "1458693352", "fax": "1458965866" }, "billing": { "name": { "first": "test", "middle": "test", "last": "test", "suffix": "t" }, "city": "roanoke", "state": "virginia", "country": "unitedstates", "address": "test", "postalCode": "24011" }, }
    When I post data for transaction /ACH/Transactions
    Then response code should be 201
    When I get data with existing ref num for transaction /Data/Transactions/ACH/ref_num
    Then response code should be 200
    And response body path status should be Batch
    And response body path service should be ACH
    Then I verify existing reference number in response

  @get_account_number
  Scenario: Verify get account number #D-36956
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 1010.14   },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    When I get data with existing ref num for transaction /Data/Transactions/Bankcard/ref_num/AccountNumber
    Then response code should be 200
    And response body path status should be Batch
    And response body path service should be Bankcard
    Then I verify existing reference number in response

  @get_account_number_ach_service
  Scenario: Verify get account number for ach service #D-36956
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to { "transactionCode": "Sale", "transactionClass": "CCD" , "originatorId": "string", "amounts": { "total": 65 }, "account": { "type": "Checking", "routingNumber": "123123123", "accountNumber": "1234567890" }, "customer": { "dateOfBirth": "2016-01-29T05:49:05.703Z", "ssn": "123456789", "license": { "number": "string", "stateCode": "VA" }, "ein": "test", "email": "achtest@gmail.com", "telephone": "1458693352", "fax": "1458965866" }, "billing": { "name": { "first": "test", "middle": "test", "last": "test", "suffix": "t" }, "city": "roanoke", "state": "virginia", "country": "unitedstates", "address": "test", "postalCode": "24011" }, }
    When I post data for transaction /ACH/Transactions
    When I get data with existing ref num for transaction /Data/Transactions/ACH/ref_num/AccountNumber
    Then response code should be 200
    And response body path status should be Batch
    And response body path service should be ACH
    Then I verify existing reference number in response

  @post_add_transaction_note
  Scenario: Verify add transaction note
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 1322.41   },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    Given I set body to {  "note": "Transaction note added"  }
    When I post data with existing ref num for transaction /Data/Transactions/ref_num/Notes
    Then response code should be 201
    And response body path status should be Approved
    #Then I verify existing reference number in response

    #Negative scenarios
  @get_transaction_details_wrong_reference
  Scenario: Verify get transaction details with wrong reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {"transactionId": "5656", "retail": {   "amounts": {      "total": 31.21   },   "authorizationCode": "565656",    "orderNumber": "5656",    "cardData": {   "number": "4111111111111111",      "expiration": "0620"  ,"cvv":"123"  },},     "transactionCode": "Sale"}
    When I post data for transaction /BankCard/Transactions
    Then response code should be 201
    And response body path status should be Approved
    And I get data with reference as abc123 for transaction /Data/Transactions
    Then response code should be 404


  @get_transaction_details_by_type_bankcard_wrong_reference
  Scenario: Verify get transaction details by type both service(bankcard) & wrong-reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data with reference as abc123 for transaction /Data/Transactions/Bankcard
    Then response code should be 404
    Then response is empty


  @get_transaction_details_by_type_ach_wrong_reference
  Scenario: Verify get transaction details by type both service(ach) & wrong-reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data with reference as abc123 for transaction /Data/Transactions/ACH
    Then response code should be 404
    Then response is empty

  @post_add_transaction_note_wrong_reference
  Scenario: Verify add transaction note with wrong reference
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    Given I set body to {  "note": "Transaction note added"  }
    When I post data for transaction /Data/Transactions/abc123/Notes
    Then response code should be 404
    And response is empty


