@intg
Feature:Verify different reports settings


  @update-notification-settings
  Scenario: Verify update notification settings
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "individualTransactions": {     "emailAddress": "sujith.ks@sonata-software.com",     "bankcardEnabled": false,     "achEnabled": true   },   "recurringTransactions": {     "emailAddress": "sujith.sasikumary@sage.com",     "bankcardEnabled": true,     "achEnabled": false   },   "expiration": {     "emailAddress": "sujith.ks@sonata-software.com",     "notificationType": "Customers"   },   "batchClose": {     "emailAddress": "sujith.ks@sonata-software.com",     "notificationType": "Jenark"   } }
    When I put data for transaction /Data/Settings/Notifications
    Then response code should be 200
    Then response is empty


  @update-notification-settings-wrong-json
  Scenario: Verify update notification settings
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to abc
    When I put data for transaction /Data/Settings/Notifications
    Then response code should be 400
    Then response body path message should be No request content was found

  @update-notification-settings-empty-json
  Scenario: Verify update notification settings empty json
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {}
    When I put data for transaction /Data/Settings/Notifications
    Then response code should be 400
    Then response body path message should be No request content was found

  @get-notification-settings
  Scenario: Verify report for get notification settings
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Settings/Notifications
    Then response code should be 200
    Then response should contain {"individualTransactions":{"emailAddress":"sujith.ks@sonata-software.com","bankcardEnabled":false,"achEnabled":true},"recurringTransactions":{"emailAddress":"sujith.sasikumary@sage.com","bankcardEnabled":true,"achEnabled":false},"expiration":{"emailAddress":"sujith.ks@sonata-software.com","notificationType":"Customers"},"batchClose":{"emailAddress":"sujith.ks@sonata-software.com","notificationType":"Jenark"}}

  @get-notification-settings-invalid-authorization
  Scenario: Verify report for get notification settings with invalid authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Settings/Notifications
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @update-risk-control-settings
  Scenario: Verify update risk control settings
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {   "cvvOptions": "None",   "avsTarget": "All",   "requireCvv": true,   "avsOptions": "None" }
    When I put data for transaction /Data/Settings/RiskControls
    Then response code should be 200
    Then response is empty


  @update-risk-control-settings-wrong-json
  Scenario: Verify update risk control  settings with wrong json
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to abc
    When I put data for transaction /Data/Settings/RiskControls
    Then response code should be 400
    Then response body path message should be No request content was found


  @update-risk-control-settings-empty-json
  Scenario: Verify update risk control  settings with empty json
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    And I set body to {}
    When I put data for transaction /Data/Settings/RiskControls
    Then response code should be 200
    Then response is empty

  @get-risk-control-settings
  Scenario: Verify report for get risk control  settings
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Settings/RiskControls
    Then response code should be 200
    Then response should contain {"cvvOptions":"None","avsTarget":"All","requireCvv":true,"avsOptions":"None"}

  @get-risk-control-settings-invalid-authorization
  Scenario: Verify report for get risk control  settings with invalid authorization
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Settings/RiskControls
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
