@intg
Feature:Verify different reports for Analytics


  @get-transaction-summary
  Scenario Outline: Verify get transaction summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/CurrentSummary/<compareType>?periods=<periodVal>
    Then response code should be 200
    Then response should contain "volume":
    Then response should contain "count":
    Then response should contain "lowestAmount":
    Then response should contain "highestAmount":
    Then response should contain "comparison":{"periodType":"<periodType>","periods":<periodVal>,"volume":{"average":
    Examples:
      | compareType         | periodVal | periodType |
      | DailyComparison     | 30        | Daily      |
      | WeeklyComparison    | 4         | Weekly     |
      | MonthlyComparison   | 3         | Monthly    |
      | QuarterlyComparison | 1         | Quarterly  |
      | YearlyComparison    | 1         | Yearly     |

  @get-transaction-summary-wrong-period
  Scenario: Verify get transaction summary with wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/CurrentSummary/DailyComparison?periods=abc@#$
    Then response code should be 200
    Then response should contain "comparison":{"periodType":"Daily","periods":30,"volume":{"average":
    Then response should contain "volume"
    Then response should contain "count"
    Then response should contain "lowestAmount"
    Then response should contain "highestAmount"
    Then response should contain "averageAmount"

  @get-transaction-summary-wrong-authorization
  Scenario: Verify get transaction summary with wrong authorization value
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/CurrentSummary/DailyComparison?periods=1
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @get-transaction-summary-bankcard-service
  Scenario Outline: Verify get transaction summary with service as bankcard
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Bankcard/CurrentSummary/<compareType>?periods=<periodVal>
    Then response code should be 200
    Then response should contain "volume":
    Then response should contain "count":
    Then response should contain "lowestAmount":
    Then response should contain "highestAmount":
    Then response should contain "comparison":{"periodType":"<periodType>","periods":<periodVal>,"volume":{"average":
    Examples:
      | compareType         | periodVal | periodType |
      | DailyComparison     | 30        | Daily      |
      | WeeklyComparison    | 4         | Weekly     |
      | MonthlyComparison   | 3         | Monthly    |
      | QuarterlyComparison | 1         | Quarterly  |
      | YearlyComparison    | 1         | Yearly     |


  @get-transaction-summary-bankcard-service-wrong-period
  Scenario: Verify get transaction summary with service as bankcard with wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Bankcard/CurrentSummary/DailyComparison?periods=abc@#$
    Then response code should be 200
    Then response should contain "comparison":{"periodType":"Daily","periods":30,"volume":

  @get-transaction-summary-wrong-authorization-bankcard-service
  Scenario: Verify get transaction summary with wrong authorization value
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Bankcard/CurrentSummary/DailyComparison?periods=1
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

  @get-transaction-summary-ach-service
  Scenario Outline: Verify get transaction summary with service as ACH
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/ACH/CurrentSummary/<compareType>?periods=<periodVal>
    Then response code should be 200
    Then response should contain "volume":
    Then response should contain "count":
    Then response should contain "lowestAmount":
    Then response should contain "highestAmount":
    Then response should contain "comparison":{"periodType":"<periodType>","periods":<periodVal>,"volume":{"average":
    Examples:
      | compareType         | periodVal | periodType |
      | DailyComparison     | 30        | Daily      |
      | WeeklyComparison    | 4         | Weekly     |
      | MonthlyComparison   | 3         | Monthly    |
      | QuarterlyComparison | 1         | Quarterly  |
      | YearlyComparison    | 1         | Yearly     |


  @get-transaction-summary-ach-service-wrong-period
  Scenario: Verify get transaction summary with service as ach with wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/ACH/CurrentSummary/DailyComparison?periods=abc@#$
    Then response code should be 200
    Then response should contain "lowestAmount"
    Then response should contain "changePercentage"
    Then response should contain "averageAmount"
    Then response should contain "comparison":{"periodType":"Daily","periods":30,"volume":{"average

  @get-transaction-summary-wrong-authorization-ach-service
  Scenario: Verify get transaction summary with wrong authorization value of ACH service
    Given I set Authorization header to abc
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/ACH/CurrentSummary/DailyComparison?periods=1
    Then response code should be 401
    Then response should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}


  @get-transaction-total-current-included-true
  Scenario Outline: Verify get transaction summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>?periods=1&includeCurrent=true
    Then response code should be 200
    Then response should contain "transactionSummary":
    Then response should contain "averageAmount":
    Then response should contain "count":
    Then response should contain "volume":
    Then response should contain {"periodType":"<periodType>","items":[
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |

  @get-transaction-total-current-included-false
  Scenario Outline: Verify get transaction summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>?periods=1&includeCurrent=false
    Then response code should be 200
    Then response should contain "transactionSummary":
    Then response should contain "averageAmount":
    Then response should contain "count":
    Then response should contain "volume":
    Then response should contain {"periodType":"<periodType>","items":[
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |

  @get-transaction-total-wrong-period-current-included-false
  Scenario: Verify get transaction summary wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/DailyTotals?periods=abc&includeCurrent=false
    Then response code should be 200
    Then response should contain {"periodType":"Daily","items":[{"startDate"

  @get-transaction-total-wrong-period-current-included-true
  Scenario: Verify get transaction total wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/DailyTotals?periods=1&includeCurrent=true
    Then response code should be 200
    Then response should contain {"periodType":"Daily","items":
    Then response should contain "startDate"
    Then response should contain "endDate"


  @get-payment-type-total-current-included-true
  Scenario Outline: Verify get payment type total included true
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>/PaymentType?periods=1&includeCurrent=true
    Then response code should be 200
    Then response should contain "transactionSummary":
    Then response should contain "averageAmount":
    Then response should contain "count":
    Then response should contain "volume":
    Then response should contain {"periodType":"<periodType>","items":[
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |

  @get-payment-type-total-current-included-false
  Scenario Outline: Verify get payment type total summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>/PaymentType?periods=1&includeCurrent=false
    Then response code should be 200
    Then response should contain "transactionSummary":
    Then response should contain "averageAmount":
    Then response should contain "count":
    Then response should contain "volume":
    Then response should contain {"periodType":"<periodType>","items":[
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |

  @get-payment-type-total-wrong-period-current-included-false
  Scenario: Verify get payment type total summary wrong period
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/DailyTotals/PaymentType?periods=abc&includeCurrent=false
    Then response code should be 200
    Then response should contain {"periodType":"Daily","items":[{"startDate"

  @get-payment-type-total-wrong-period-current-included-true
  Scenario: Verify get payment type total summary
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/DailyTotals/PaymentType?periods=1&includeCurrent=true
    Then response code should be 200
    Then response should contain {"periodType":"Daily","items":[{"startDate":"2018-01-26T00:00:00-05:00","endDate":"2018-01-26T00:00:00-05:00","volume":0.0,"count":0}],"transactionSummary":{"averageAmount":0.0,"count":0.0,"volume":0.0}}

  @get-batch-total-current-included-true
  Scenario Outline: Verify get batch total  summary currentIncluded as true
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>/BatchTotals?periods=<periodVal>&includeCurrent=true
    Then response code should be 200
    Then response should contain "service":
    Then response should contain "startDate":
    Then response should contain "endDate":
    Then response should contain "volume":
    Then response should contain "count":
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |
      | YearlyTotals    | abc@#$    | Yearly     |

  @get-batch-total-current-included-false
  Scenario Outline: Verify get batch total  summary currentIncluded as false
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/<totalType>/BatchTotals?periods=<periodVal>&includeCurrent=false
    Then response code should be 200
    Then response should contain "service":
    Then response should contain "startDate":
    Then response should contain "endDate":
    Then response should contain "volume":
    Then response should contain "count":
    Examples:
      | totalType       | periodVal | periodType |
      | DailyTotals     | 30        | Daily      |
      | WeeklyTotals    | 4         | Weekly     |
      | MonthlyTotals   | 3         | Monthly    |
      | QuarterlyTotals | 1         | Quarterly  |
      | YearlyTotals    | 1         | Yearly     |


  @get-posting-type-totals-invalid-date-field
  Scenario: Verify get posting type total  with invalid date fields
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Totals/PostingType?startDate=abc&endDate=def
    Then response code should be 200
    Then response should contain {"startDate":
    Then response should contain "endDate":
    Then response should contain debitCount
    Then response should contain debitTotal
    Then response should contain creditCount
    Then response should contain creditTotal



  @get-posting-type-totals-empty-date-field
  Scenario: Verify get posting type total  with empty date fields
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Totals/PostingType
    Then response code should be 200
    Then response should contain {"startDate":
    Then response should contain "endDate":
    Then response should contain debitCount
    Then response should contain debitTotal
    Then response should contain creditCount
    Then response should contain creditTotal

  @get-posting-type-totals-date-field
  Scenario: Verify get posting type total  with correct  date fields
    Given I set Authorization header to Authorization
    And I set content-type header to application/json
    When I get data for transaction /Data/Analytics/Totals/PostingType?startDate=2017-12-01&endDate=2017-12-30
    Then response code should be 200
    Then response should contain {"startDate":"2017-12-01T00:00:00","endDate":"2017-12-30T00:00:00"
    Then response should contain debitCount
    Then response should contain debitTotal
    Then response should contain creditCount
    Then response should contain creditTotal


